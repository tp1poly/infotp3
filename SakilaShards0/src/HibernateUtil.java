import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.shards.ShardId;
import org.hibernate.shards.ShardedConfiguration;
import org.hibernate.shards.cfg.ConfigurationToShardConfigurationAdapter;
import org.hibernate.shards.loadbalance.RoundRobinShardLoadBalancer;
import org.hibernate.shards.strategy.ShardStrategy;
import org.hibernate.shards.strategy.ShardStrategyFactory;
import org.hibernate.shards.strategy.ShardStrategyImpl;
import org.hibernate.shards.strategy.access.ParallelShardAccessStrategy;
import org.hibernate.shards.strategy.access.SequentialShardAccessStrategy;
import org.hibernate.shards.strategy.access.ShardAccessStrategy;
import org.hibernate.shards.strategy.resolution.AllShardsShardResolutionStrategy;
import org.hibernate.shards.strategy.resolution.ShardResolutionStrategy;
import org.hibernate.shards.strategy.selection.RoundRobinShardSelectionStrategy;
import org.hibernate.shards.strategy.selection.ShardSelectionStrategy;


public class HibernateUtil {
    
    private static SessionFactory sessionFactory;
    final static Map<Integer, Integer> virtualShardMap = buildVirtualShardMap();
   
    static {
        try {
        	Configuration config = new Configuration();
            config.configure("shard0.hibernate.cfg.xml");
            config.addResource("Film.hbm.xml");
            List shardConfigs = new ArrayList();
        //    List<Configuration> shardConfigs = new ArrayList<Configuration>();
            shardConfigs.add(new ConfigurationToShardConfigurationAdapter(new Configuration().configure("shard0.hibernate.cfg.xml")));
            shardConfigs.add(new ConfigurationToShardConfigurationAdapter(new Configuration().configure("shard1.hibernate.cfg.xml")));
            shardConfigs.add(new ConfigurationToShardConfigurationAdapter(new Configuration().configure("shard2.hibernate.cfg.xml")));
            ShardStrategyFactory shardStrategyFactory = buildShardStrategyFactory();
            ShardedConfiguration shardedConfig = new ShardedConfiguration(config, shardConfigs, shardStrategyFactory, virtualShardMap);
            sessionFactory = shardedConfig.buildShardedSessionFactory();
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
        	ex.printStackTrace();
        	sessionFactory = null;
           // System.err.println("Initial SessionFactory creation failed.  " + ex);
           // throw new ExceptionInInitializerError(ex);
        }
    }
    // Sequentielle
  /*  static ShardStrategyFactory buildShardStrategyFactory() {
        ShardStrategyFactory shardStrategyFactory = new ShardStrategyFactory() {
            public ShardStrategy newShardStrategy(List shardIds) {
                RoundRobinShardLoadBalancer loadBalancer = new RoundRobinShardLoadBalancer(shardIds);
                ShardSelectionStrategy pss = new RoundRobinShardSelectionStrategy(loadBalancer);
                ShardResolutionStrategy prs = new AllShardsShardResolutionStrategy(shardIds);
                ShardAccessStrategy pas = new SequentialShardAccessStrategy();
                return new ShardStrategyImpl(pss, prs, pas);
            }
        };
        return shardStrategyFactory;
    } */
    // Fin sequentielle
  
    // Paralelle
    static ShardStrategyFactory buildShardStrategyFactory() {
    	ThreadFactory factory = new ThreadFactory() {
    		public Thread newThread(Runnable r) {
    			Thread t = Executors.defaultThreadFactory().newThread(r);
    			t.setDaemon(true);
    			return t;
    		}
    	};

    	final ThreadPoolExecutor exec = new ThreadPoolExecutor(10, 50, 60, TimeUnit.SECONDS, new SynchronousQueue<Runnable>(), factory);

    	ShardStrategyFactory shardStrategyFactory = new ShardStrategyFactory() {
    		public ShardStrategy newShardStrategy(List<ShardId> shardIds) {
    			ShardSelectionStrategy pss = new VariantShardSelectionStrategy();
    			ShardResolutionStrategy prs = new VariantShardResolutionStrategy(shardIds);
    			ShardAccessStrategy pas = new ParallelShardAccessStrategy(exec);
    			return new ShardStrategyImpl(pss, prs, pas);
    		}
    	};
    	return shardStrategyFactory;
    }  
    // Fin paralelle
    
    
    private static Map<Integer, Integer> buildVirtualShardMap() {
    	//	Year year = null;
            final int ShardId0 = 0;
            final int ShardId1 = 1;
            final int ShardId2 = 2;
            Map<Integer, Integer> virtualShardMap = new HashMap<Integer, Integer>();            
            virtualShardMap.put(Year.Y2006.getCode(), ShardId0);        
            virtualShardMap.put(Year.Y2007.getCode(), ShardId0);
            virtualShardMap.put(Year.Y2008.getCode(), ShardId0);        
            virtualShardMap.put(Year.Y2009.getCode(), ShardId1);
            virtualShardMap.put(Year.Y2010.getCode(), ShardId1);
            virtualShardMap.put(Year.Y2011.getCode(), ShardId1);
            virtualShardMap.put(Year.Y2012.getCode(), ShardId2);
            virtualShardMap.put(Year.Y2013.getCode(), ShardId2);
            virtualShardMap.put(Year.Y2014.getCode(), ShardId2);
            virtualShardMap.put(Year.Y2015.getCode(), ShardId2);

            return virtualShardMap;       
        }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }  
    
}
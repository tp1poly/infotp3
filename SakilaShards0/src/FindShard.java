
public enum FindShard {
	shard0(0),
	shard1(1),
	shard2(2),
	shard3(3),
	shard4(4),
	shard5(5),
	shard6(6),
	shard7(7),
	shard8(8);
	
	private int code;
	FindShard(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
	
	public  static int fromInt(int value) {    
		switch(value) {
		case 0: return 0;
		case 1: return 1;
		case 2: return 2;
		case 3: return 3;
		case 4: return 4;
		case 5: return 5;
		case 6: return 6;
		case 7: return 7;
		case 8: return 8;

		default:
			throw new RuntimeException("Unknown type");
		}
	}
	
	public  static int fromCustom(int value) {
		value = find(value);
		switch(value) {
		case 0: return 0;
		case 1: return 1;
		case 2: return 2;
		case 3: return 3;
		case 4: return 4;
		case 5: return 5;
		case 6: return 6;
		case 7: return 7;
		case 8: return 8;

		default:
			throw new RuntimeException("Unknown type");
		}
	}
	
	public static int find(int value) {
		if(value > 0 & value <= 333) {
			if (value > 0 & value <= 111) {
				return 0;
			}else 
				if (value > 111 && value < 222 ) {
					return  1;
				}else {return 2;}
		}else 
			if (value > 333 & value <= 666 ) {
				if (value > 333 & value <= 444 ) {
					return 3;
				}else 
					if (value > 444 & value < 555 ) {
						return 4;
					}else {return 5;}
			}else {
				if (value > 666 & value <= 777 ) {
					return 6;
				}else 
					if (value > 777 & value < 888 ) {
						return 7;
					}else {return 8;}
			}  				
	}
}

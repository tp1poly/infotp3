import org.hibernate.shards.ShardId;
import org.hibernate.shards.strategy.selection.ShardSelectionStrategy;
public class VariantShardSelectionStrategyHash implements ShardSelectionStrategy {
	public ShardId selectShardIdForNewObject(Object obj) {
        if (obj instanceof Rental) {
            int shardId = 0;
            int customer = ((Rental) obj).getCustomerId();
            String customerToHash = customer + "customerId";
                shardId = FindHash.find(Md5.getHash((customerToHash)));
           // System.out.println("customer: " +customer +" "+ shardId +" " + Md5.getHash(Integer.toString(customer)));
            return new ShardId(shardId);
            
        }
        throw new IllegalArgumentException();
    }
} 
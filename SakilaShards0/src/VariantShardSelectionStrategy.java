/*import org.hibernate.shards.ShardId;
import org.hibernate.shards.strategy.selection.ShardSelectionStrategy;

public class VariantShardSelectionStrategy implements ShardSelectionStrategy {
    public ShardId selectShardIdForNewObject(Object obj) {
        // temporary single shard
    	
        return new ShardId(0);
    }
} */

import org.hibernate.shards.ShardId;
import org.hibernate.shards.strategy.selection.ShardSelectionStrategy;
public class VariantShardSelectionStrategy implements ShardSelectionStrategy
{
    public ShardId selectShardIdForNewObject(Object obj) {
        if (obj instanceof Film) {
            int shardId = 0;
            int year = ((Film) obj).getReleaseYear();
            if(year == Year.fromIntYear(year))
                shardId = Year.fromInt(year);
          //  System.out.println("customer: " +year +" "+ shardId);
            return new ShardId(shardId);
        }
        throw new IllegalArgumentException();
    }
} 
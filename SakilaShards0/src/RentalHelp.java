import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


public class RentalHelp {
	Session session = null;
	SessionFactory sessionFactory = null;
	

    public RentalHelp() {
    	SessionFactory sessionFactory = HibernateUtilLook.getSessionFactory();
    	this.session = sessionFactory.openSession();
    	
    }
    
    public void stopSession() {
    	session.close();
    }

    public void getRentalTitles(int nomReg, int startID, int endID) {
    	Session session = null;
    	SessionFactory sessionFactory = HibernateUtilLook.getSessionFactory();		
    	session = sessionFactory.openSession();
        List<Rental> rentalList = null;
        try {
            Transaction tx = session.beginTransaction();
            for ( int i=0; i < nomReg; i++ ) {
            Query q = session.createQuery("from Rental as rental where rental.customerId between '" + startID + "' and '" + endID + "'");
            rentalList = (List<Rental>) q.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // printRental(rentalList);
        // return rentalList;
    }  

    public Rental getRentalByID(int rentalId) {
    	Session session = null;
    	SessionFactory sessionFactory = HibernateUtilLook.getSessionFactory();		
    	session = sessionFactory.openSession();
        Rental rental = null;

        try {
            Transaction tx = session.beginTransaction();
            Query q = session.createQuery("from Rental as rental where rental.customerId=" + rentalId);
            rental = (Rental) q.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rental;
    } 
    
    public void  getRentalIdHash(int nomReg, int startId, int endId) {
    	Rental rental = new Rental();

    	Session session = null;
    	SessionFactory sessionFactory = HibernateUtilHash.getSessionFactory();		
    	session = sessionFactory.openSession();
    	if (startId == endId) {
    		rental.setCustomerId(startId);
    	}
        List<Rental> rentalList = null;
        try {
        	for ( int i=0; i < nomReg; i++ ) {
            Transaction tx = session.beginTransaction();
            Query q = session.createQuery("from Rental as rental where rental.customerId between '" + startId + "' and '" + endId + "'");
            rentalList = (List<Rental>) q.list();
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
        // printRental(rentalList);
        // return rentalList;
    }
    
    public void  getRentalId(int nomReg, int startId, int endId) {
    	Rental rental = new Rental();

    	Session session = null;
    	SessionFactory sessionFactory = HibernateUtilLook.getSessionFactory();		
    	session = sessionFactory.openSession();
    	if (startId == endId) {
    		rental.setCustomerId(startId);
    	}
        List<Rental> rentalList = null;
        try {
            Transaction tx = session.beginTransaction();
            for ( int i=0; i < nomReg; i++ ) {
            Query q = session.createQuery("from Rental as rental where rental.customerId between '" + startId + "' and '" + endId + "'");
            rentalList = (List<Rental>) q.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // printRental(rentalList);
        // return rentalList;
    }
    
    public void printRental (List<Rental> rentalList) {
        for (Iterator iterator = rentalList.iterator(); iterator.hasNext();){
			Rental rental = (Rental) iterator.next(); 
			System.out.print("Id: " + rental.getRentalId());
			System.out.print("  Inventory: " + rental.getInventoryId());
			System.out.println("  Customer: " + rental.getCustomerId()); 

		}
    }
  
    
    public void insertRental (int nomReg, int customerId) {
    	Session session = null;
		if (customerId == 0) {
			customerId = ((int)(Math.random()*1000 + 1));
		}    	
    	SessionFactory sessionFactory = HibernateUtilLook.getSessionFactory();		
    	session = sessionFactory.openSession();    	
    	try {
    		Transaction tx = session.beginTransaction();   		
    		for ( int i=0; i < nomReg; i++ ) {   
    			Rental rental = new Rental();
    			int inventoryId = ((int)(Math.random()*2000 + 1));   			
    			Date date = new Date();
    		//	System.out.print("Rental Id: " + i + "******");

    			rental.setRentalDate(date);
    			rental.setInventoryId(inventoryId);
    			rental.setCustomerId(customerId);
    			rental.setReturnDate(date);
    			rental.setStaffId((byte) 2);
    			rental.setLastUpdate(date);

    			session.save(rental);
    		}
    		tx.commit();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}        					
    }
    
    public void insertRentalHash (int nomReg, int customerId) {
    	Session session = null;
		if (customerId == 0) {
			customerId = ((int)(Math.random()*1000 + 1));
		}    	
    	SessionFactory sessionFactory = HibernateUtilHash.getSessionFactory();		
    	session = sessionFactory.openSession();    	
    	try {
    		Transaction tx = session.beginTransaction();   		
    		for ( int i=0; i < nomReg; i++ ) {   
    			Rental rental = new Rental();
    			int inventoryId = ((int)(Math.random()*2000 + 1));   			
    			Date date = new Date();
    		//	System.out.print("Rental Id: " + i + "******");

    			rental.setRentalDate(date);
    			rental.setInventoryId(inventoryId);
    			rental.setCustomerId(customerId);
    			rental.setReturnDate(date);
    			rental.setStaffId((byte) 2);
    			rental.setLastUpdate(date);

    			session.save(rental);
    		}
    		tx.commit();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}        					
    }
    
    
}

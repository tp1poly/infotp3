
public enum FindHash {
	shard0(0),
	shard1(1),
	shard2(2),
	shard3(3),
	shard4(4),
	shard5(5),
	shard6(6),
	shard7(7),
	shard8(8);
	
	private int code;
	FindHash(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
	
	public  static int fromInt(int value) {    
		switch(value) {
		case 0: return 0;
		case 1: return 1;
		case 2: return 2;
		case 3: return 3;
		case 4: return 4;
		case 5: return 5;
		case 6: return 6;
		case 7: return 7;
		case 8: return 8;

		default:
			throw new RuntimeException("Unknown type");
		}
	}
	
	public  static int fromCustom(int value) {
		value = find(value);
		switch(value) {
		case 0: return 0;
		case 1: return 1;
		case 2: return 2;
		case 3: return 3;
		case 4: return 4;
		case 5: return 5;
		case 6: return 6;
		case 7: return 7;
		case 8: return 8;

		default:
			throw new RuntimeException("Unknown type");
		}
	}
	
	public static int find(int value) {
		int Mshard0 = 124487119;
		int Mshard6 = 128450133;
		int Mshard8 = 338724111;
		int Mshard1 = 558743399;
		int Mshard4 = 586096336;
		int Mshard3 = 651761276;
		int Mshard5 = 651797817;
		int Mshard7 = 724444845;
		int Mshard2 = 865687030;

		if(value >= Mshard0 & value < Mshard6) { return 6;} else		
		if(value >= Mshard6 & value < Mshard8) { return 8;} else
		if(value >= Mshard8 & value < Mshard1) { return 1;} else
		if(value >= Mshard1 & value < Mshard4) { return 4;} else
		if(value >= Mshard4 & value < Mshard3) { return 3;} else
		if(value >= Mshard3 & value < Mshard5) { return 5;} else
		if(value >= Mshard5 & value < Mshard7) { return 7;} else
		if(value >= Mshard7 & value < Mshard2) { return 2;} else
		if(value >= Mshard2 & value < Mshard0) { return 0;} else	
		{ return 0;}

	}
}

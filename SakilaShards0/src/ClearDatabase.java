import java.io.File;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.impl.SessionImpl;

import com.mysql.jdbc.Connection;


public class ClearDatabase {
	public ClearDatabase() {
		
	}
	private static final SessionFactory sessionFactory;
    static {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
        	Configuration configuration = new Configuration().configure(new File("shard0.hibernate.cfg.xml"));
        	configuration.setProperty("hibernate.hbm2ddl.auto","create");
            sessionFactory = configuration.buildSessionFactory();
            
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
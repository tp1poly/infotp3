import java.io.PrintStream;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;

public class MainShards {
	private static Scanner clavier = new Scanner(System.in);
	FilmHelp helper;
	int startId;
	int endId;
	List filmTitles;
	
	
	public MainShards(int startId, int endId) {
		helper = new FilmHelp();
		this.startId = startId;
		this.endId = endId;
	}
	private static void pHelp (PrintStream out)
    {
		out.println(
                "1. Film   - Faire une consultation par Id" +
                "2. Film   - Faire une consultation par annee\n" +
                "3. Film   - Inserer des registres\n" +                    
                "4. Rental - Faire une consultation par Id\n" +
                "5. Rental - Inserer des registres\n" +
                "6. Rental - Faire une consultation par Id\n" +
                "7. Rental - Faire une consultation par Id Hash\n" +
                "8. Rental - Inserer des registres Hash\n" +
                "  help                     Print this summary\n");
    }
			
	public static void main(String[] args) {
		
		double time_start, time_end;		
	                        
        if (args.length > 0 ) {        	           
        	if (args[0].equals("1")) {
        		FilmHelp ME1 = new FilmHelp();
        		time_start = System.currentTimeMillis();
        		ME1.getFilmTitles(Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]));
        		time_end = System.currentTimeMillis();
            //	System.out.println("the task has taken for " +args[1] + " Reg. "+ ( (time_end - time_start)/1000 ) +" seconds");
            	System.out.println(args[1] +",Req," +( (time_end - time_start)/1000 ) +",seconds");
        	}else
        	if (args[0].equals("3")) {
        		FilmHelp ME2 = new FilmHelp();
        		time_start = System.currentTimeMillis();
        		ME2.insertFilm(Integer.parseInt(args[1]), Integer.parseInt(args[2]));
        		time_end = System.currentTimeMillis();
            //	System.out.println("the task has taken for " +args[1] + " Reg. "+ ( (time_end - time_start)/1000 ) +" seconds");
            	System.out.println(args[1] +",Req," +( (time_end - time_start)/1000 ) +",seconds");
        	}else
        	if (args[0].equals("2")) {
        		FilmHelp ME3 = new FilmHelp();
        		time_start = System.currentTimeMillis();
        		ME3.getFilmYear(Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]));
        		time_end = System.currentTimeMillis();
            //	System.out.println("the task has taken for " +args[1] + " Reg. "+ ( (time_end - time_start)/1000 ) +" seconds");
            	System.out.println(args[1] +",Req," +( (time_end - time_start)/1000 ) +",seconds");
        	}else
        	if (args[0].equals("4")) {
        		RentalHelp ME4 = new RentalHelp();
        		time_start = System.currentTimeMillis();
        		ME4.getRentalTitles(Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]));
        		time_end = System.currentTimeMillis();
            //	System.out.println("the task has taken for " +args[1] + " Reg. "+ ( (time_end - time_start)/1000 ) +" seconds");
            	System.out.println(args[1] +",Req," +( (time_end - time_start)/1000 ) +",seconds");
        	}else
        	if (args[0].equals("5")) {
        		RentalHelp ME5 = new RentalHelp();
        		time_start = System.currentTimeMillis();
        		ME5.insertRental(Integer.parseInt(args[1]), Integer.parseInt(args[2]));
        		time_end = System.currentTimeMillis();
            //	System.out.println("the task has taken for " +args[1] + " Reg. "+ ( (time_end - time_start)/1000 ) +" seconds");
            	System.out.println(args[1] +",Req," +( (time_end - time_start)/1000 ) +",seconds");
        	}else
        	if (args[0].equals("6")) {
        		RentalHelp ME6 = new RentalHelp();
        		time_start = System.currentTimeMillis();
        		ME6.getRentalId(Integer.parseInt(args[1]), Integer.parseInt(args[2]),  Integer.parseInt(args[3]));
        		time_end = System.currentTimeMillis();
            //	System.out.println("the task has taken for " +args[1] + " Reg. "+ ( (time_end - time_start)/1000 ) +" seconds");
            	System.out.println(args[1] +",Req," +( (time_end - time_start)/1000 ) +",seconds");
        	}else
        	if (args[0].equals("7")) {
        		RentalHelp ME7 = new RentalHelp();
        		time_start = System.currentTimeMillis();
        		ME7.getRentalIdHash(Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]));
        		time_end = System.currentTimeMillis();
            //	System.out.println("the task has taken for " +args[1] + " Reg. "+ ( (time_end - time_start)/1000 ) +" seconds");
            	System.out.println(args[1] +",Req," +( (time_end - time_start)/1000 ) +",seconds");
        	}else
        	if (args[0].equals("8")) {
        		RentalHelp ME5 = new RentalHelp();
        		time_start = System.currentTimeMillis();
        		ME5.insertRentalHash(Integer.parseInt(args[1]), Integer.parseInt(args[2]));
        		time_end = System.currentTimeMillis();
            //	System.out.println("the task has taken for " +args[1] + " Reg. "+ ( (time_end - time_start)/1000 ) +" seconds");
            	System.out.println(args[1] +",Req," +( (time_end - time_start)/1000 ) +",seconds");
        	}else {
        		
        	}
        } else {
        	//PrintStream out = null;
        	//System.out.println(args[0]+" "+ args[1]+" "+ args[2] +" " +args[2] + " "+args.length);
            System.err.println("Usage: java SakilaShards0 <test> <No. Reg.> <Id1> <Id2>\n");
            System.err.println(
                    "1. Film   - Faire une consultation par Id\n" +
                    "2. Film   - Faire une consultation par annee\n" +
                    "3. Film   - Inserer des registres\n" +                    
                    "4. Rental - Faire une consultation par Id\n" +
                    "5. Rental - Inserer des registres\n" +
                    "6. Rental - Faire une consultation par Id\n" +
                    "7. Rental - Faire une consultation par Id Hash\n" +
                    "8. Rental - Inserer des registres Hash\n");
               //     "help                     Print this summary\n");
            System.exit(-1);
        }
        	
		/*
		// ****************************
		
		System.out.println("1. Film - Faire une consultation par Id");
		System.out.println("2. Film - Faire une consultation par annee");
        System.out.println("3. Film - Inserer des registres");
        
        System.out.println("4. Rental - Faire une consultation par Id");
        System.out.println("5. Rental - Inserer des registres");
        System.out.println("6. Rental - Faire une consultation par Id");
        System.out.println("7. Rental - Faire une consultation par Id Hash");
        System.out.println("8. Rental - Inserer des registres Hash");
     //   System.out.println("9. Clear les Base de donnees");
        int option = clavier.nextInt();
     // Find Film for Film Id
        if (option == 1) {  
        	System.out.println("Tapez nom Reg: ");        	        
        	int nomReg = clavier.nextInt();
        	System.out.println("Tapez l'Id 1: ");        	        
        	int startFilm = clavier.nextInt();
        	System.out.println("Tapez l'Id 2: ");        	        
        	int endFilm = clavier.nextInt();
        	FilmHelp ME1 = new FilmHelp();
    		ME1.getFilmTitles(nomReg, startFilm, endFilm);
        }
        
     // Insert n Film registres with year
        if (option == 3) {
        	System.out.println("Nombre de Reg: ");        
        	int startFilm = clavier.nextInt();
        	System.out.println("Tapez l'anne du Film: ");        
        	int endFilm = clavier.nextInt();
        	FilmHelp ME2 = new FilmHelp();
        	ME2.insertFilm(startFilm, endFilm);
        }
        
     // Find Film for 
        if (option == 2) {
        	System.out.println("Tapez nom Reg: ");        	        
        	int nomReg = clavier.nextInt();
        	System.out.println("Tapez l'annee de debut: ");        
        	int startFilm = clavier.nextInt();
        	System.out.println("Tapez l'annee du fin: ");        
        	int endFilm = clavier.nextInt();
        	FilmHelp ME3 = new FilmHelp();
        	ME3.getFilmYear(nomReg, startFilm, endFilm);
        }
        
        if (option == 4) {   
        	System.out.println("Tapez nom Reg: ");        	        
        	int nomReg = clavier.nextInt();
        	System.out.println("Tapez l'Id 1: ");        
        	int startFilm = clavier.nextInt();
        	System.out.println("Tapez l'Id 2: ");        
        	int endFilm = clavier.nextInt();
        	RentalHelp ME4 = new RentalHelp();
    		ME4.getRentalTitles(nomReg, startFilm, endFilm);
        }
        
     // Insert n Film registres with year
        if (option == 5) {
        	System.out.println("Tapez le nombre de Reg: ");        
        	int nomReg = clavier.nextInt();
        	System.out.println("Tapez le costumer Id (0= rand): ");        
        	int customerId = clavier.nextInt();
        	RentalHelp ME5 = new RentalHelp();
        	ME5.insertRental(nomReg, customerId);
        }
        
     // Find Film for 
        if (option == 6) {
        	System.out.println("Tapez nom Reg: ");        	        
        	int nomReg = clavier.nextInt();
        	System.out.println("Tapez l'Id 1: ");        
        	int startFilm = clavier.nextInt();
        	System.out.println("Tapez l'Id 2: ");        
        	int endFilm = clavier.nextInt();
        	RentalHelp ME6 = new RentalHelp();
        	ME6.getRentalId(nomReg, startFilm, endFilm);
        }
        
        if (option == 8) {
        	//double time_start, time_end;
        	System.out.println("Tapez le nombre de Reg: ");        
        	int nomReg = clavier.nextInt();
        	System.out.println("Tapez le costumer Id (0= rand): ");        
        	int customerId = clavier.nextInt();        	
        	RentalHelp ME8 = new RentalHelp();
        	time_start = System.currentTimeMillis();
        	ME8.insertRentalHash(nomReg, customerId);
        	time_end = System.currentTimeMillis();
        	System.out.println("the task has taken for " +nomReg + " Reg. "+ ( (time_end - time_start)/1000 ) +" seconds");
        }
        
     // Find Film for 
        if (option == 7) {
        	System.out.println("Tapez nom Reg: ");        	        
        	int nomReg = clavier.nextInt();
        	System.out.println("Tapez l'Id 1: ");        
        	int startFilm = clavier.nextInt();
        	System.out.println("Tapez l'Id 2: ");        
        	int endFilm = clavier.nextInt();
        	RentalHelp ME7 = new RentalHelp();
        	ME7.getRentalIdHash(nomReg, startFilm, endFilm);
        } 
		
		// *********
           */             
	}

}

public enum Year {
	Y2006(0),
	Y2007(1),
	Y2008(2),
	Y2009(3),
	Y2010(4),
	Y2011(5),
	Y2012(6),
	Y2013(7),
	Y2014(8),
	Y2015(9);

	private int code;
	Year(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	// the valueOfMethod
	public  static int fromInt(int value) {    
		switch(value) {
		case 2006: return 0;
		case 2007: return 1;
		case 2008: return 2;
		case 2009: return 3;
		case 2010: return 4;
		case 2011: return 5;
		case 2012: return 6;
		case 2013: return 7;
		case 2014: return 8;
		case 2015: return 9;

		default:
			throw new RuntimeException("Unknown type");
		}
	}
	
	// the valueOfMethod
		public  static int fromIntYear(int value) {    
			switch(value) {
			case 2006: return 2006;
			case 2007: return 2007;
			case 2008: return 2008;
			case 2009: return 2009;
			case 2010: return 2010;
			case 2011: return 2011;
			case 2012: return 2012;
			case 2013: return 2013;
			case 2014: return 2014;
			case 2015: return 2015;

			default:
				throw new RuntimeException("Unknown type");
			}
		}

	public String toString() {
		switch(this) {
		case Y2006:
			return "2006";
		case Y2007:
			return "2007";
		case Y2008:
			return "2008";
		case Y2009:
			return "2009";
		case Y2010:
			return "2010";
		case Y2011:
			return "2011";
		case Y2012:
			return "2012";
		case Y2013:
			return "2013";
		case Y2014:
			return "2014";
		case Y2015:
			return "2015";
		default:
			return "Unknown";
		}
	}

}

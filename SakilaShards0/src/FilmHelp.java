import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


public class FilmHelp {
	Session session = null;
	SessionFactory sessionFactory = null;
	

    public FilmHelp() {
    	SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    	this.session = sessionFactory.openSession();
    	
    }
    
    public void stopSession() {
    	session.close();
    }

    public void getFilmTitles(int nomReg, int startID, int endID) {
    	Session session = null;
    	SessionFactory sessionFactory = HibernateUtil.getSessionFactory();		
    	session = sessionFactory.openSession();
        List<Film> filmList = null;
        try {
            Transaction tx = session.beginTransaction();
            for ( int i=0; i < nomReg; i++ ) {
            Query q = session.createQuery("from Film as film where film.filmId between '" + startID + "' and '" + endID + "'");
            filmList = (List<Film>) q.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // printFilm(filmList);
        // return filmList;
    }  

    public Film getFilmByID(int filmId) {
    	Session session = null;
    	SessionFactory sessionFactory = HibernateUtil.getSessionFactory();		
    	session = sessionFactory.openSession();
        Film film = null;

        try {
            Transaction tx = session.beginTransaction();
            Query q = session.createQuery("from Film as film where film.filmId=" + filmId);
            film = (Film) q.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return film;
    } 
    
    public void  getFilmYear(int nomReg, int startYear, int endYear) {
    	Film film = new Film();

    	Session session = null;
    	SessionFactory sessionFactory = HibernateUtil.getSessionFactory();		
    	session = sessionFactory.openSession();
    	if (startYear == endYear) {
    		film.setReleaseYear(startYear);
    	}
        List<Film> filmList = null;
        try {
            Transaction tx = session.beginTransaction();
            for ( int i=0; i < nomReg; i++ ) {
            Query q = session.createQuery("from Film as film where film.releaseYear between '" + startYear + "' and '" + endYear + "'");
            filmList = (List<Film>) q.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // printFilm(filmList);
        // return filmList;
    }
    
    public void printFilm (List<Film> filmList) {
        for (Iterator iterator = filmList.iterator(); iterator.hasNext();){
			Film film = (Film) iterator.next(); 
			System.out.print("Id: " + film.getFilmId());
			System.out.print("  Release: " + film.getReleaseYear());
			System.out.print("  Title: " + film.getTitle()); 
			System.out.println("  Description: " + film.getDescription()); 
		}
    }
  
    
    public void insertFilm (int nomReg, int year) {
    	Session session = null;
    	SessionFactory sessionFactory = HibernateUtil.getSessionFactory();		
    	session = sessionFactory.openSession();    	
    	try {
    		Transaction tx = session.beginTransaction();
    		for ( int i=0; i < nomReg; i++ ) {
    			Film film = new Film();
    			Date date = new Date();
    		//	System.out.print("Film Id: " + i + "******");
    			final BigDecimal bDec = new BigDecimal("0.52");
    			final BigDecimal bDec2 = new BigDecimal("20.52");
    			film.setTitle("ACADEMY DINOSAUR");
    			film.setDescription("A Epic Drama of a Feminist And a Mad");
    			film.setReleaseYear(year);
    			film.setLanguageId((byte) 1);
    			film.setOriginalLanguageId((byte) 1);
    			film.setRentalDuration((byte) 6);
    			film.setRentalRate(bDec);
    			film.setLength((short) 86);
    			film.setReplacementCost(bDec2);
    			film.setRating("PG");
    			film.setSpecialFeatures("Deleted Scenes,Behind the Scenes");
    			film.setLastUpdate(date);
    			session.save(film);
    		}
    		tx.commit();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}        					
    }
    
    
}

import java.util.HashMap;
import java.util.Map;
import java.util.Collections;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.shards.ShardId;
import org.hibernate.shards.ShardedConfiguration;
import org.hibernate.shards.cfg.ConfigurationToShardConfigurationAdapter;
import org.hibernate.shards.cfg.ShardConfiguration;
import org.hibernate.shards.strategy.ShardStrategy;
import org.hibernate.shards.strategy.ShardStrategyFactory;
import org.hibernate.shards.strategy.ShardStrategyImpl;
import org.hibernate.shards.strategy.access.SequentialShardAccessStrategy;
import org.hibernate.shards.strategy.resolution.AllShardsShardResolutionStrategy;
import org.hibernate.shards.strategy.selection.ShardSelectionStrategy;


public class StandardSectionFactoryHelper {
	
	private Map<Integer, Integer> buildVirtualShardMap() {
	//	Year year = null;
        final int ShardId0 = 0;
        final int ShardId1 = 1;
        final int ShardId2 = 2;
        Map<Integer, Integer> virtualShardMap = new HashMap<Integer, Integer>();            
        virtualShardMap.put(Year.Y2006.getCode(), ShardId0);        
        virtualShardMap.put(Year.Y2007.getCode(), ShardId0);
        virtualShardMap.put(Year.Y2008.getCode(), ShardId0);        
        virtualShardMap.put(Year.Y2009.getCode(), ShardId1);
        virtualShardMap.put(Year.Y2010.getCode(), ShardId1);
        virtualShardMap.put(Year.Y2011.getCode(), ShardId1);
        virtualShardMap.put(Year.Y2012.getCode(), ShardId2);
        virtualShardMap.put(Year.Y2013.getCode(), ShardId2);
        virtualShardMap.put(Year.Y2014.getCode(), ShardId2);
        virtualShardMap.put(Year.Y2015.getCode(), ShardId2);

        return Collections.unmodifiableMap(virtualShardMap);       
    }
		    
}

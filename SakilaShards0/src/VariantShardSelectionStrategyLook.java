import org.hibernate.shards.ShardId;
import org.hibernate.shards.strategy.selection.ShardSelectionStrategy;
public class VariantShardSelectionStrategyLook implements ShardSelectionStrategy {
	public ShardId selectShardIdForNewObject(Object obj) {
        if (obj instanceof Rental) {
            int shardId = 0;
            int customer = ((Rental) obj).getCustomerId();
                shardId = FindShard.find(customer);
          //  System.out.println("customer: " +customer +" "+ shardId + FindShard.find(customer) );
            return new ShardId(shardId);
            
        }
        throw new IllegalArgumentException();
    }
} 
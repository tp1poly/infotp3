import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.shards.ShardId;
import org.hibernate.shards.ShardedConfiguration;
import org.hibernate.shards.cfg.ConfigurationToShardConfigurationAdapter;
import org.hibernate.shards.loadbalance.RoundRobinShardLoadBalancer;
import org.hibernate.shards.strategy.ShardStrategy;
import org.hibernate.shards.strategy.ShardStrategyFactory;
import org.hibernate.shards.strategy.ShardStrategyImpl;
import org.hibernate.shards.strategy.access.ParallelShardAccessStrategy;
import org.hibernate.shards.strategy.access.SequentialShardAccessStrategy;
import org.hibernate.shards.strategy.access.ShardAccessStrategy;
import org.hibernate.shards.strategy.resolution.AllShardsShardResolutionStrategy;
import org.hibernate.shards.strategy.resolution.ShardResolutionStrategy;
import org.hibernate.shards.strategy.selection.RoundRobinShardSelectionStrategy;
import org.hibernate.shards.strategy.selection.ShardSelectionStrategy;


public class HibernateUtilLook {
    
    private static SessionFactory sessionFactory;
    final static Map<Integer, Integer> virtualShardMap = buildVirtualShardMap();
   
    static {
        try {
        	Configuration config = new Configuration();
            config.configure("shard0.hibernate.cfg.xml");
            config.addResource("Rental.hbm.xml");
            List shardConfigs = new ArrayList();
        //    List<Configuration> shardConfigs = new ArrayList<Configuration>();
            shardConfigs.add(new ConfigurationToShardConfigurationAdapter(new Configuration().configure("shard0.hibernate.cfg.xml")));
            shardConfigs.add(new ConfigurationToShardConfigurationAdapter(new Configuration().configure("shard1.hibernate.cfg.xml")));
            shardConfigs.add(new ConfigurationToShardConfigurationAdapter(new Configuration().configure("shard2.hibernate.cfg.xml")));
            ShardStrategyFactory shardStrategyFactory = buildShardStrategyFactory();
            ShardedConfiguration shardedConfig = new ShardedConfiguration(config, shardConfigs, shardStrategyFactory, virtualShardMap);
            //ShardedConfiguration shardedConfig = new ShardedConfiguration(config, shardConfigs, shardStrategyFactory);
            sessionFactory = shardedConfig.buildShardedSessionFactory();
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
        	ex.printStackTrace();
        	sessionFactory = null;
           // System.err.println("Initial SessionFactory creation failed.  " + ex);
           // throw new ExceptionInInitializerError(ex);
        }
    }
    // Sequentielle
  /*  static ShardStrategyFactory buildShardStrategyFactory() {
        ShardStrategyFactory shardStrategyFactory = new ShardStrategyFactory() {
            public ShardStrategy newShardStrategy(List shardIds) {
                RoundRobinShardLoadBalancer loadBalancer = new RoundRobinShardLoadBalancer(shardIds);
                ShardSelectionStrategy pss = new RoundRobinShardSelectionStrategy(loadBalancer);
                ShardResolutionStrategy prs = new AllShardsShardResolutionStrategy(shardIds);
                ShardAccessStrategy pas = new SequentialShardAccessStrategy();
                return new ShardStrategyImpl(pss, prs, pas);
            }
        };
        return shardStrategyFactory;
    } */
    // Fin sequentielle
  
    // Paralelle
    static ShardStrategyFactory buildShardStrategyFactory() {
    	ThreadFactory factory = new ThreadFactory() {
    		public Thread newThread(Runnable r) {
    			Thread t = Executors.defaultThreadFactory().newThread(r);
    			t.setDaemon(true);
    			return t;
    		}
    	};

    	final ThreadPoolExecutor exec = new ThreadPoolExecutor(10, 50, 60, TimeUnit.SECONDS, new SynchronousQueue<Runnable>(), factory);

    	ShardStrategyFactory shardStrategyFactory = new ShardStrategyFactory() {
    		public ShardStrategy newShardStrategy(List<ShardId> shardIds) {
    			ShardSelectionStrategy pss = new VariantShardSelectionStrategyLook();
    			ShardResolutionStrategy prs = new VariantShardResolutionStrategy(shardIds);
    			ShardAccessStrategy pas = new ParallelShardAccessStrategy(exec);
    			return new ShardStrategyImpl(pss, prs, pas);
    		}
    	};
    	return shardStrategyFactory;
    }  
    // Fin paralelle
    
    
    private static Map<Integer, Integer> buildVirtualShardMap() {
    	//	Year year = null;
            final int ShardId0 = 0;
            final int ShardId1 = 1;
            final int ShardId2 = 2;
            Map<Integer, Integer> virtualShardMap = new HashMap<Integer, Integer>();            
            
            virtualShardMap.put(FindShard.shard0.getCode(), ShardId0);        
            virtualShardMap.put(FindShard.shard1.getCode(), ShardId0);
            virtualShardMap.put(FindShard.shard2.getCode(), ShardId0);        
            virtualShardMap.put(FindShard.shard3.getCode(), ShardId1);
            virtualShardMap.put(FindShard.shard4.getCode(), ShardId1);
            virtualShardMap.put(FindShard.shard5.getCode(), ShardId1);
            virtualShardMap.put(FindShard.shard6.getCode(), ShardId2);
            virtualShardMap.put(FindShard.shard7.getCode(), ShardId2);
            virtualShardMap.put(FindShard.shard8.getCode(), ShardId2);

            return virtualShardMap;       
        }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }  
    
}
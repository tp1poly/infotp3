
Benchmarking No. 1 

sysbench 0.5:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 8
Random number generator seed is 0 and will be ignored


Threads started!

OLTP test statistics:
    queries performed:
        read:                            291158
        write:                           83188
        other:                           41594
        total:                           415940
    transactions:                        20797  (346.57 per sec.)
    read/write requests:                 374346 (6238.34 per sec.)
    other operations:                    41594  (693.15 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0073s
    total number of events:              20797
    total time taken by event execution: 479.8768s
    response time:
         min:                                  4.51ms
         avg:                                 23.07ms
         max:                                307.13ms
         approx.  95 percentile:              37.21ms

Threads fairness:
    events (avg/stddev):           2599.6250/1.87
    execution time (avg/stddev):   59.9846/0.01


Benchmarking No. 2 

sysbench 0.5:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 8
Random number generator seed is 0 and will be ignored


Threads started!

OLTP test statistics:
    queries performed:
        read:                            285670
        write:                           81620
        other:                           40810
        total:                           408100
    transactions:                        20405  (340.03 per sec.)
    read/write requests:                 367290 (6120.49 per sec.)
    other operations:                    40810  (680.05 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0099s
    total number of events:              20405
    total time taken by event execution: 479.9650s
    response time:
         min:                                  4.45ms
         avg:                                 23.52ms
         max:                                304.71ms
         approx.  95 percentile:              41.84ms

Threads fairness:
    events (avg/stddev):           2550.6250/3.31
    execution time (avg/stddev):   59.9956/0.01


Benchmarking No. 3 

sysbench 0.5:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 8
Random number generator seed is 0 and will be ignored


Threads started!

OLTP test statistics:
    queries performed:
        read:                            285614
        write:                           81604
        other:                           40802
        total:                           408020
    transactions:                        20401  (339.97 per sec.)
    read/write requests:                 367218 (6119.53 per sec.)
    other operations:                    40802  (679.95 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0075s
    total number of events:              20401
    total time taken by event execution: 479.9408s
    response time:
         min:                                  4.70ms
         avg:                                 23.53ms
         max:                                264.39ms
         approx.  95 percentile:              40.61ms

Threads fairness:
    events (avg/stddev):           2550.1250/3.79
    execution time (avg/stddev):   59.9926/0.01


Benchmarking No. 4 

sysbench 0.5:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 8
Random number generator seed is 0 and will be ignored


Threads started!

OLTP test statistics:
    queries performed:
        read:                            282702
        write:                           80772
        other:                           40386
        total:                           403860
    transactions:                        20193  (336.49 per sec.)
    read/write requests:                 363474 (6056.89 per sec.)
    other operations:                    40386  (672.99 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0100s
    total number of events:              20193
    total time taken by event execution: 479.9425s
    response time:
         min:                                  4.86ms
         avg:                                 23.77ms
         max:                                213.21ms
         approx.  95 percentile:              42.37ms

Threads fairness:
    events (avg/stddev):           2524.1250/4.96
    execution time (avg/stddev):   59.9928/0.01


Benchmarking No. 5 

sysbench 0.5:  multi-threaded system evaluation benchmark

Running the test with following options:
Number of threads: 8
Random number generator seed is 0 and will be ignored


Threads started!

OLTP test statistics:
    queries performed:
        read:                            285222
        write:                           81492
        other:                           40746
        total:                           407460
    transactions:                        20373  (339.53 per sec.)
    read/write requests:                 366714 (6111.56 per sec.)
    other operations:                    40746  (679.06 per sec.)
    ignored errors:                      0      (0.00 per sec.)
    reconnects:                          0      (0.00 per sec.)

General statistics:
    total time:                          60.0033s
    total number of events:              20373
    total time taken by event execution: 479.9167s
    response time:
         min:                                  5.01ms
         avg:                                 23.56ms
         max:                                190.40ms
         approx.  95 percentile:              43.25ms

Threads fairness:
    events (avg/stddev):           2546.6250/5.07
    execution time (avg/stddev):   59.9896/0.01

